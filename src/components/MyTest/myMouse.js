import { useState } from 'react';

const MyMouse = props => {  // zmiana na myMouse
    const [mousePos, setMousePos] = useState({ x: 0 }, { y: 0 })

    const handleMouseMove = el => {
        setMousePos({ x: el.clientX, y: el.clientY })
    }

    return (
        <div className="col-12 destination" onMouseMove={(el) => handleMouseMove(el)}>
            <h1>Rusz myszką</h1>
            {/* <p>pozycja myszki: x:{mousePos.x},y:{mousePos.y}</p>  tu trzeb wyrenderować inny element*/}
            {
                props.render && props.render(mousePos)
            }
        </div>
    )
}

export default MyMouse;

