import './styles.css';
import React, { useContext, useEffect, useRef, Suspense } from 'react';
//////import User from '../../components/User';
//import Oponent from './../../components/Oponent';
import Header from './../../components/Header';
//na początku importujemy UserStatsProvider
import { UserStatsProvider, OponentStatsProvider, CharsStats, GameState, CommonStatsProvider } from './../../context';
//import Common from './../../components/Common';

import Form from './../../components/Form';
//import MyTest from './../../components/MyTest';
//import MyButton from './../../components/'


////const Common=React.lazy(()=>import('../../components/Common/'))
//aby dzialalo dynamiczne ładowanie, trzeba owrapować conajmniej common, a najlepiej cały kod w Suspense

const Homepage = () => {

    //możemy wyświetlić parametry z CharsStats
    const { name, str, hp, speed, addStr } = useContext(CharsStats);
    const { isName, setName } = useContext(GameState);
    const refPo = useRef(null);
    const refL = useRef(null);
    const refD = useRef(null);
    const refPu = useRef(null);
    const refG = useRef(null);



    useEffect(() => {
        console.log('homepage: ', isName)
        const getName = localStorage.getItem('name');
        if (getName != null) {
            if (getName.length > 0) { setName(true) }
        };
    }, [isName])

    // function callback(
    //     id, phase, actualDuration,baseDuration,startTime,commitTime,interactions
    // ) {
    //     console.log(id, phase, actualDuration,baseDuration,startTime,commitTime,interactions);
    // }

    return (
        <div >
            {isName ? (
                <div  >
                    <Suspense fallback={<div><h1>ładowanie</h1></div>}>
                    <div>
                        <Header {...{ refPo, refL, refD, refPu, refG }} />
                    </div>
                    {/* <div>
                        <MyTest />
                    </div> */}
                    <div className="col-12 destination" ref={refPo}>Polana</div>
                    <div className="col-12 destination" ref={refL}>Las</div>
                    <div className="col-12 destination" ref={refD}>Dżungla</div>
                    <div className="col-12 destination" ref={refPu}>Pustynia</div>
                    <div className="col-12 destination" ref={refG}>Góry</div>
                    <div className="game__wrapper">
                        {/* <Profiler id="UserStatsProvider" onRender={callback}> */}
                        <CommonStatsProvider nazwa='User'>
                            {/* <Common /> */}
                        </CommonStatsProvider>

                        <CommonStatsProvider nazwa='Oponent'  >
                            {/* <Common alive /> */}
                        </CommonStatsProvider>
                    </div>
                    </Suspense>
                </div>
            ) : (
                <Form />
            )}
        </div>
    );
    // jeśli nie stworzonego providera,(np. dla drugiego Usera), to pobieran sa domyslne wartości z 
    // charsStat. Wartości w CharsStat bedą się zmieniać. UserStatsProvider bedzie miał inne, a OponentStatsProvide
    // będzie mial inne jeśli owprapowaliśmy w provider, to z niego 
    //będą pobierane wartośći.







}

export default Homepage;