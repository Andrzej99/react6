import './styles.css';
import React from "react";
// w index1 pokazano sposób bez mapowania

const scrollToRef = ref => window.scrollTo(0, ref.current.offsetTop);

const Header = ({ refPo, refL, refD, refPu, refG }) => {
    const navList = [
        { 'name': 'Polana', 'ref': refPo },
        { 'name': 'Las', 'ref': refL },
        { 'name': 'Dżungla', 'ref': refD },
        { 'name': 'Pustynia', 'ref': refPu },
        { 'name': 'Góry', 'ref': refG }
    ];


    return (
        <div className="header-menu">
            <ul className="ul">
                {
                    navList.map(el => {
                        return <li className="liItem " key={el.name} onClick={() => scrollToRef(el.ref)}>{el.name} </li>
        
                    })
                }
            </ul>
        </div>
    )
}



// class Header extends React.Component {
//     render() {
//         return (
//             <div>
//                 <h1>To jest kurs react-klasa,witam Cię {this.props.name}</h1>
//             </div>
//         )    
//     }
// }

export default Header;