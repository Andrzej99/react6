import React from "react";

const scrollToRef = ref => window.scrollTo(0,ref.current.offsetTop);

const Header = ({ refPo, refL, refD, refPu, refG }) => {
    return (
        <div>
            <ul>
                <li  onClick={()=>scrollToRef(refPo)}>Polana</li>
                <li  onClick={()=>scrollToRef(refL)}>Las</li>
                <li  onClick={()=>scrollToRef(refD)}>Dżungla</li>
                <li  onClick={()=>scrollToRef(refPu)}>Pustynia</li>
                <li  onClick={()=>scrollToRef(refG)}>Góry</li>
            </ul>
        </div>
    )
}



// class Header extends React.Component {
//     render() {
//         return (
//             <div>
//                 <h1>To jest kurs react-klasa,witam Cię {this.props.name}</h1>
//             </div>
//         )    
//     }
// }

export default Header;