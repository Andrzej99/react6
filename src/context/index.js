import React, { createContext, useState } from 'react';  //useState jest hookiem

// export const ComponentType=createContext({
//     type:"Oponent",
//     changeType:item=>{},
// })

export const GameState = createContext({
    isName: false,
    setName: item => { }
})

export const GameApp = ({ children }) => {
    const [isName, setName] = useState(false);
    return (
        <GameState.Provider
            value={{
                isName,
                setName
            }}
        >
            {children}
        </GameState.Provider>
    )
}


export const CharsStats = createContext({
    name: "Demon",
    str: 0,
    hp: 0,
    speed: 10,
    dmg: 0,
    lvl: 1,
    type: 'User',
    addStr: item => { },
    addName: item => { },
    changeType: item => { }
})



export const CommonStatsProvider = ({nazwa, children }) => {

    const [type, changeType] = useState(nazwa);
    const [name, addName] = useState('');
//    console.log('type CommonStatsProvider :', type);
 //   console.log('jestem wCommonStatsProvider');
    //const name='Test';
    const str = 10;
    const hp = 10;
    const speed = 2;
    const dmg = str * speed;
    const lvl = 1;
    return (
        <CharsStats.Provider
            value={{
                name,
                str,
                hp,
                speed,
                addName,
                dmg,
                lvl,
                type,
                changeType
            }}
        >
            {children} 
        </CharsStats.Provider>

    )
}