import { useState } from 'react';
import './myMouse';
import MyMouse from './myMouse';
import MyTxt from './myTxt';

const MyTest = props => {

    return (
        <div>
            <MyMouse render={mousePos => (
                <MyTxt mousePos={mousePos}/>
            )} 
             />
             
            <MyMouse/>
        </div>
    )
    /*    
    to już niepotrzebne po imporcie myMouse
        const [mousePos, setMousePos] = useState({ x: 0 }, { y: 0 })
    
        const handleMouseMove=el=> {
            setMousePos({x:el.clientX,y:el.clientY})
        }
    
        return (
            <div className="col-12 destination" onMouseMove={(el)=>handleMouseMove(el)}>
                <h1>Rusz myszką</h1>
                <p>pozycja myszki: x:{mousePos.x},y:{mousePos.y}</p>
            </div>
        )*/
}

export default MyTest;