import { enumStringMember } from '@babel/types';
import React, { Component, useState, useRef } from 'react';
import styled from 'styled-components';
import RectangleResult from './rectangleResult';

const MyRectangle = styled.div`
width: ${props => props.width + 'px'};
height: ${props => props.height + 'px'};
background-color: blue;
display: flex;
text-align: center;
align-items: center;
justify-content: center;
transition: all 0.5s;
`;


const Rectangle = () => {

    const [width, setWidth] = useState(103);
    const [height, setHeight] = useState(120);
    const [maxS] = useState(500);
    const [minS] = useState(100);
    const ref = useRef(null);
    const refW = useRef(null);
    const refH = useRef(null);

    const handleResize = () => {
        const max = maxS;
        const min = minS;
        let width = Math.floor(Math.random() * (max - min)) + min;
        let height = Math.floor(Math.random() * (max - min)) + min;
        setWidth(width);
        setHeight(height);
    }

    function packToArray() {
        let arr=[];
        arr.push({ width });
        arr.push({ height })
        return arr;
    }

    return (
        <div className="rectangle__wrapper">
            <MyRectangle ref={ref}

                width={width}
                height={height}
            >
                <RectangleResult{...packToArray()} />
            </MyRectangle>
            <p>Szerokość: {width} px</p>
            <p>Wysokość: {height} px</p>
            <button onClick={handleResize}>Generuj nowy element</button>
        </div>
    )
}

export default Rectangle;

