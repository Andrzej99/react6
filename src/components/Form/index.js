import React, {useContext,useState} from "react";
import {GameState} from './../../context';
//usuwamy props
const Form = () => {
    const {isName,setName}=useContext(GameState);
    const [name,addName]=useState('');
//        localStorage.removeItem('name');


    const handleSubmit=(e)=> {
        e.preventDefault();
        setName(true);
        console.log('Form isName:',isName);
        localStorage.setItem('name',name);
    }
//    console.log("name:",name);
    return (
        <form onSubmit={e=>handleSubmit(e)}>
            <label>Imię:</label>
            <input name="name" type="text" value={name} onChange={(e)=>addName(e.target.value) }/>
            <input name="send" type="submit" />
        </form>
    )
}

export default Form;

/* 
Komponent funkcyjny nie może mieć this, wobec czego przenosimy formę do komponentu klasowego
*/



// class Form extends React.Component {
//     constructor(props) {    //ustawiamy wstępnie value
//         super(props);
//         this.state = { value: 'Piotr' };  //jeśli value jest puste, to nie możemy nic wpisać do inputa
//         //jeśli wpiszemy np. Piotr do value, to nie można go zmienić.
//         //dzięki temu możemy tworzyć formularze, które mają niezmialne pola, np user.
//         //Aby można było zmienic, to możemy wstawić jako default lub użyć onchange
//         this.handleChange=this.handleChange.bind(this);
//         this.handleSubmit=this.handleSubmit.bind(this);
//     }
//     handleChange(e) {
//         this.setState({ value: e.target.value });
//     }
// /*
// Aby formularz nie wysylał się automatycznie, trzeba zatrzymać formularz przed wyslaniem
// */
// handleSubmit(e) {
//     e.preventDefault();
// }
//     render() {
//         return (
//             <form onSubmit={this.handleSubmit}>
//                 <label>Imię:</label>
//                 <input name="name" type="text" value={this.state.value} onChange={this.handleChange}/>
//                 <input name="send" type="submit" />
//             </form>
//         )
//     }
// }

